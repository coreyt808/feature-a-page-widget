<?php
if( !class_exists('FPW_Widget') ) :
class FPW_Widget extends WP_Widget {

	// widget actual processes
	public function __construct() {
		parent::__construct(
	 		'fpw_widget', // Base ID
			__( 'Feature a Page Widget', 'fapw' ), // Name
			array( 'description' => __( 'A widget to feature a single page', 'fapw' ) )
		);
	}

	/**
	* Options for the widget
	*/
 	public function form( $instance ) {
		
		// Some default values for the widgets
		$defaults = array(
			'hide_page_title' => 0,
			'title' => '',
			'featured_page_id' => '',
			'layout' => 'banner',
			'image_id' => ''
		);
		// any options not set get the default
		$instance = wp_parse_args( $instance, $defaults );
		// extract them for cleaner code
		extract( $instance, EXTR_SKIP );

		// prepare list of pages
		$post_statuses = apply_filters('fpw_admin_post_statuses', array('publish'));
		$hierarchical  = apply_filters('fpw_admin_hierarchical', 0);
		$args = apply_filters('fpw_admin_get_pages_args', array(
			'hierarchical' => $hierarchical,
			'post_status' => $post_statuses,
			'posts_per_page' => -1
		) );

		if ( isset( $args['post_type'] ) ) {
			$pages_array = get_posts( $args );
		}
		else {
			$pages_array = get_pages( $args );	
		}
		$page_select_list = array( '' => '' );
		foreach( $pages_array as $page ){
			$page_select_list[$page->ID] = esc_attr( apply_filters('the_title', $page->post_title) );
		}

		// widget admin form begins
		?>

		<p class="fpw-widget-title">
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">
				<?php esc_attr_e( 'Widget Title', 'fapw' ); ?>
			</label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>" />
		</p>

		<p class="fpw-featured-page-id">
			<label for="<?php echo $this->get_field_id( 'featured_page_id' ); ?>" class="fpw-widget-heading">
				<?php esc_attr_e( apply_filters('fpw_featured_page_label','Page to feature:'), 'fapw' ); ?>
			</label>
			<select class="fpw-page-select" data-placeholder="<?php _e( apply_filters('fpw_featured_page_placeholder','Select Featured Page&hellip;'), 'fapw' ); ?>" id="<?php echo $this->get_field_id( 'featured_page_id' ); ?>" name="<?php echo $this->get_field_name('featured_page_id'); ?>">
				<?php foreach( $page_select_list as $page_id => $page_title ) {
					// We'll give people a hint if the excerpt and image are set
					$featured_image = null; $excerpt = null;
					if( has_post_thumbnail( $page_id ) )
						$featured_image = 'featured-image';
					if( has_excerpt( $page_id) )
						$excerpt = 'excerpt';
					printf( '<option class="%4$s %5$s" value="%1$s" %3$s>%2$s</option>',
						$page_id,
						$page_title,
						selected( $featured_page_id, $page_id, false ),
						$featured_image,
						$excerpt
					);
				} ?>
			</select>
		</p>
		<p class="fpw-widget-page-title">
			<label><?php _e(apply_filters('fpw_featured_page_hide_title_label', 'Hide Page Title'), 'fpw')?></label>
			<input type="checkbox" class="fpw-page-title_choice" id="<?php echo $this->get_field_id( 'hide_page_title' ); ?>" name="<?php echo $this->get_field_name('hide_page_title'); ?>" value="1" <?php checked( '1', $hide_page_title ); ?> />
		</p>

		<p class="fpw-widget-image">
			<?php image_upload_helper(array(
				'label' => 'a custom image',
				'thumbnail_id' => $image_id,
				'size' => 'thumbnail',
				'field_name' => $this->get_field_name('image_id'),
			));?>
		</p>

		<p class="fpw-layouts">
			<span class="fpw-widget-heading"><?php _e( 'Layout Options:', 'fapw' ); ?></span>
			<input type="radio" class="fpw-layout-banner" id="<?php echo $this->get_field_id( 'layout-banner' ); ?>" name="<?php echo $this->get_field_name('layout'); ?>" value="banner" <?php checked( 'banner', $layout ); ?> />
			<label for="<?php echo $this->get_field_id( 'layout-banner' ); ?>">
				<?php esc_attr_e( 'Banner Image', 'fapw' ); ?>
			</label><br />
			<input type="radio" class="fpw-layout-wrapped" id="<?php echo $this->get_field_id( 'layout-wrapped' ); ?>" name="<?php echo $this->get_field_name('layout'); ?>" value="wrapped" <?php checked( 'wrapped', $layout ); ?> />
			<label for="<?php echo $this->get_field_id( 'layout-wrapped' ); ?>">
				<?php esc_attr_e( 'Wrapped Image', 'fapw' ); ?>
			</label><br />
			<input type="radio" class="fpw-layout-big" id="<?php echo $this->get_field_id( 'layout-big' ); ?>" name="<?php echo $this->get_field_name('layout'); ?>" value="big" <?php checked( 'big', $layout ); ?> />
			<label for="<?php echo $this->get_field_id( 'layout-big' ); ?>">
				<?php esc_attr_e( 'Big Image', 'fapw' ); ?>
			</label>

		</p>

		<?php

	}

	// processes widget options to be saved
	public function update( $new_instance, $old_instance ) {
		$instance = wp_parse_args( $new_instance, $old_instance );

		$instance['hide_page_title'] = empty($new_instance['hide_page_title']) ? 0 : 1;

		// Sanitize all the args
		$instance['title'] = sanitize_text_field( $new_instance['title'] );

		// the featured id should be an int
		$instance['featured_page_id'] = (int) $new_instance['featured_page_id'];

		// the custom thumnail id
		$instance['image_id'] = (int) $new_instance['image_id'];

		// validate layout against accepted options
		$accepted_layouts = array(
			'banner',
			'big',
			'wrapped'
		);
		$instance['layout'] = esc_attr( $new_instance['layout'] );
		if ( ! in_array( $instance['layout'], $accepted_layouts ) )
			$instance['layout'] = '';
		
		return $instance;
	}

	// outputs the content of the widget
	public function widget( $args, $instance ) {
		
		/** 
		 * Extract, sanitize, and compile attributes
		 *----------------------------------------------------*/

		// lets get lots of awesome values to work with
		extract($args);
		extract($instance);

		// without an ID, we're done.
		if( ! $featured_page_id )
			return;
		$featured_page_id = apply_filters('fpw_widget_featured_page_id', $featured_page_id);
		$featured_page = get_post( $featured_page_id );

		// if the ID didn't return a page, we're also done
		if( ! $featured_page )
			return;

		if( $title ) {
			$title = apply_filters( 'widget_title', sanitize_text_field($title) );	
			$title = sprintf('<a href="%s">%s</a>', get_permalink($featured_page_id), $title);
		}
		// Let's make a post_class string
		$post_class = get_post_class( 'hentry fpw-clearfix fpw-layout-' . esc_attr( $layout ), (int) $featured_page_id );
		$post_classes = '';
		foreach ($post_class as $class) {
			$post_classes .= $class . ' ';
		}

		// see if there's a page title. if so, put it together nicely for use in the widget
		if ( empty($hide_page_title) && $featured_page->post_title ) {
			$page_title = apply_filters( 'fpw_page_title', esc_attr( apply_filters('the_title', $featured_page->post_title ) ) );
			$page_title_html = sprintf( 
				'<h2 class="fpw-page-title entry-title">%1$s</h2>',
				sanitize_text_field( $page_title )
			);
		} else {
			$page_title = null;
			$page_title_html = null;
		}
		// if there's an excerpt, grab it and filter
		if( !empty($featured_page->post_excerpt )) {
			if( class_exists( 'RichTextExcerpts' ) ) {
				$excerpt = wp_kses_decode_entities( $featured_page->post_excerpt );	
			}
			else {
				$excerpt = esc_html( $featured_page->post_excerpt );
				$excerpt = apply_filters( 'fpw_excerpt', $excerpt, $featured_page_id, $featured_page->post_excerpt );
			}
		} else {
				$excerpt = apply_filters('fpw_excerpt', $featured_page->post_content, $featured_page_id);
		}
		//$excerpt = apply_filters('the_content', $excerpt);

		// the featured image size is dependant on layout
		switch ($layout) {
			case 'wrapped':
				$image_size = 'fpw_square';
				break;

			case 'big':
				$image_size = 'fpw_big';
				break;

			case 'banner':
				$image_size = 'fpw_banner';
				break;
			
			default:
				$image_size = 'fpw_square';
				break;
		}
		$image_size = apply_filters('fpw_image_size', $image_size, $featured_page_id);
		// see if there is a post_thumbnail grab it and filter it
		if ($image_id) {
			$featured_image = wp_get_attachment_image($image_id, $image_size);
			$featured_image = apply_filters( 'fpw_featured_image', $featured_image, $featured_page_id );
		}
		else if ( has_post_thumbnail( $featured_page_id ) ) {
			$featured_image = get_the_post_thumbnail( $featured_page_id, $image_size );
			$featured_image = apply_filters( 'fpw_featured_image', $featured_image, $featured_page_id );
		} else {
			$featured_image = null;
		}
		/** 
		 * Widget Output Begins
		 *----------------------------------------------------*/

		// use a version of the widget template from the theme if it exists
		// otherwise, use the default widget template from the plugin folder
		$fpw_template = locate_template( 'fpw_views/fpw_default.php', false, true );
		if( $fpw_template ) {
			require( $fpw_template );
		} else {
			require( plugin_dir_path( __FILE__ ) . 'fpw_views/fpw_default.php' );
		}

	}

} #end FPW_Widget

class FPW_Widget2 extends FPW_Widget {

	// widget actual processes
	public function __construct() {
		WP_Widget::__construct(
	 		'fpw_widget2', // Base ID
			__( 'Feature a Post Widget', 'fapw' ), // Name
			array( 'description' => __( 'A widget to feature a post page', 'fapw' ) )
		);
	}

	public function form($instance) {
		add_filter('fpw_admin_get_pages_args', array($this, 'get_pages_args'), 10, 2);
		add_filter('fpw_featured_page_hide_title_label', array($this, 'fpw_featured_page_hide_title_label'));
		add_filter('fpw_featured_page_placeholder', array($this, 'fpw_featured_page_placeholder'));
		add_filter('fpw_featured_page_label', array($this, 'fpw_featured_page_label'));
		parent::form($instance);
		remove_filter('fpw_admin_get_pages_args', array($this, 'get_pages_args'), 10, 2);
		remove_filter('fpw_featured_page_hide_title_label', array($this, 'fpw_featured_page_hide_title_label'));
		remove_filter('fpw_featured_page_placeholder', array($this, 'fpw_featured_page_placeholder'));
		remove_filter('fpw_featured_page_label', array($this, 'fpw_featured_page_label'));
	}
	function fpw_featured_page_hide_title_label($label) {
		return 'Hide Post Title';
	}

	function fpw_featured_page_label($label) {
		return 'Select Featured Post';
	}
	function fpw_featured_page_placeholder($placeholder) {
		return 'Select Featured Post&hellip;';
	}

	public function get_pages_args( $args ) {
		$args['post_type'] = 'post';
		return $args;
	}



}
endif;